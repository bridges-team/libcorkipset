/* -*- coding: utf-8 -*-
 * ----------------------------------------------------------------------
 * Copyright © 2017, libcorkipset authors
 * All rights reserved.
 *
 * Please see the COPYING file in this distribution for license details.
 * ----------------------------------------------------------------------
 */

#ifndef LIBCORKIPSET_IPSET_H
#define LIBCORKIPSET_IPSET_H

#warning "<libcorkipset/ipset.h> is deprecated; use <libcork/ipset.h> instead"
#include <libcork/ipset.h>

#endif  /* LIBCORKIPSET_IPSET_H */
